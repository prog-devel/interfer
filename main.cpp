#include <QGuiApplication>

#include <QQmlEngine>
#include <QQmlFileSelector>
#include <QQuickView>
#include <QDir>

#include "canv.h"


int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterType<Canv>("OpenGLUnderQML", 1, 0, "Canv");

    QQuickView view;

    new QQmlFileSelector(view.engine(), &view);
    QObject::connect(view.engine(), SIGNAL(quit()), &app, SLOT(quit()));

#ifdef USE_QRC
    view.setSource(QUrl("qrc:///root.qml"));
#else
    view.setSource(QUrl("root.qml"));
#endif

    view.setResizeMode(QQuickView::SizeRootObjectToView);
    view.show();

    return app.exec();
}
