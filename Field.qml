import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.2

ColumnLayout {
    property string label: "Label"
    property string suff

    property alias minVal: val.minimumValue
    property alias maxVal: val.maximumValue
    property alias step: val.stepSize
    //property alias suff: val.suffix
    property alias val: val.value

    Label {
        id: label
        text: parent.label + ": " + val.value.toPrecision(4) + " " + parent.suff
    }

    Slider {
        id: val

        Layout.fillWidth: true
    }

}
