uniform sampler2D u_tex0;
varying highp vec4 v_texc;

void main(void)
{
    gl_FragColor = texture2D(u_tex0, vec2(v_texc.x, 0));
}

