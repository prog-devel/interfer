attribute highp vec4 a_vert;
attribute highp vec4 a_texc;
uniform highp mat4 u_mvp_matrix;
varying highp vec4 v_texc;

void main(void)
{
    gl_Position = u_mvp_matrix * a_vert;
    v_texc = a_texc;
}
