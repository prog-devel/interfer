#include "canv.h"

#include <QtMath>
#include <QtQuick/qquickwindow.h>
#include <QtGui/QOpenGLShaderProgram>
#include <QtGui/QOpenGLFramebufferObject>
#include <QtGui/QOpenGLVertexArrayObject>
#include <QtGui/QOpenGLContext>


class CanvRenderer Q_DECL_FINAL: public Canv::Renderer {
public:
    CanvRenderer() : m_u_mvp_matrix(-1) {}

    QOpenGLFramebufferObject *createFramebufferObject(const QSize &) Q_DECL_OVERRIDE;
    void synchronize(QQuickFramebufferObject *) Q_DECL_OVERRIDE;
    void render() Q_DECL_OVERRIDE;

private:
    void initProgram();
    void updateGeometry(const QSize &sz);
    void updateInterfer(Canv *cv);

    void attachVertices();

    QSharedPointer<QOpenGLShaderProgram> m_program;
    QSharedPointer<QOpenGLTexture> m_tex;

    QMatrix4x4 m_mvpMatrix;
    int m_u_mvp_matrix;
};


#define DEFINE_SETTER(type, pname, Pname) \
    void Canv::set##Pname(type pname) \
    { \
        if (pname == m_##pname) \
            return; \
        m_##pname = pname; \
        emit pname##Changed(); \
        update(); \
    }

Canv::Canv()
    : m_intencity(0)
    , m_gamma(0)
    , m_d(0)
    , m_l(0)
{
}

Canv::Renderer *Canv::createRenderer() const
{
    return new CanvRenderer;
}

DEFINE_SETTER(qreal, intencity, Intencity)
DEFINE_SETTER(qreal, gamma, Gamma)
DEFINE_SETTER(qreal, d, D)
DEFINE_SETTER(qreal, l, L)


QOpenGLFramebufferObject *CanvRenderer::createFramebufferObject(const QSize & size)
{
    qDebug() << "Create FBO";

    initProgram();
    updateGeometry(size);

    QOpenGLFramebufferObjectFormat fmt;
    fmt.setMipmap(false);
    fmt.setTextureTarget(GL_TEXTURE_2D);
    return new QOpenGLFramebufferObject(size, fmt);
}

void CanvRenderer::synchronize(QQuickFramebufferObject * item)
{
    qDebug() << "Synchronize";

    auto cv = dynamic_cast<Canv *>(item);
    Q_ASSERT(cv);

    updateInterfer(cv);
}

void CanvRenderer::render()
{
    if (!m_tex || !m_program)
        return;

    qDebug() << "Render";

    auto f = QOpenGLContext::currentContext()->functions();

    f->glClearColor(0, 1, 0, 1); // for test
    f->glClear(GL_COLOR_BUFFER_BIT);

    m_program->bind();
    m_program->setUniformValue(m_u_mvp_matrix, m_mvpMatrix);

    m_tex->bind();

    attachVertices();
    f->glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    m_program->release();
}

void CanvRenderer::initProgram()
{
    if (!!m_program) return;

    m_program.reset(new QOpenGLShaderProgram());

    bool ret = true;

#ifdef USE_QRC
    ret &= m_program->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/interfer.vsh");
    ret &= m_program->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/interfer.fsh");
#else
    ret &= m_program->addShaderFromSourceFile(QOpenGLShader::Vertex, "interfer.vsh");
    ret &= m_program->addShaderFromSourceFile(QOpenGLShader::Fragment, "interfer.fsh");
#endif
    if (!ret)
        qFatal("Can't load shader(s)");

    m_program->bindAttributeLocation("a_vert", 0);
    m_program->bindAttributeLocation("a_texc", 1);

    ret = m_program->link();

    if (!ret)
        qFatal("Can't link shader program");

    m_u_mvp_matrix = m_program->uniformLocation("u_mvp_matrix");
}

void CanvRenderer::attachVertices()
{
    static float verts[] = {
        0, 0,
        1, 0,
        0, 1,
        1, 1
    };

    m_program->enableAttributeArray(0);
    m_program->setAttributeArray(0, GL_FLOAT, verts, 2);

    m_program->enableAttributeArray(1);
    m_program->setAttributeArray(1, GL_FLOAT, verts, 2);
}

void CanvRenderer::updateGeometry(const QSize &)
{
    m_mvpMatrix.setToIdentity();
    m_mvpMatrix.ortho(0, 1, 0, 1, -1, 1);
}

void CanvRenderer::updateInterfer(Canv *cv)
{
    qDebug() << "Update Interfer";

    QImage img(cv->width(), 1, QImage::Format_ARGB32);

    auto I0 = cv->intencity(); // 0.0 ... 1.0
    auto Imax = 4.0 * 1.0; // 1.0 - max intencity
    auto L = cv->l();
    auto d = cv->d();
    auto gamma = cv->gamma() * 10e-9;// 390 ... 770 нМ
    auto k = 2*M_PI / gamma;

    auto getPointColor = [&](int x) -> QColor {
        auto ds = x * d / L;
        auto I = 2*I0 * (1 + qCos(k * ds));
        int Ii = I / Imax * 255;

        return QColor(Ii, Ii, Ii, 255);
    };

    for (int x = 0; x < cv->width(); ++x) {
        img.setPixel(x, 0, getPointColor(x).rgba());
    }

    m_tex.reset(new QOpenGLTexture(img, QOpenGLTexture::DontGenerateMipMaps));
}
