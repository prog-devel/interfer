import QtQuick 2.0
import QtQuick.Layouts 1.2

import OpenGLUnderQML 1.0

Item {
    id: root

    width: 400
    height: 400

    ColumnLayout {
        anchors.margins: 6
        anchors.fill: parent

        spacing: 10

        Canv {
            id: canv

            anchors.left: parent.left
            anchors.right: parent.right

            height: 100

            intencity: intencity.val
            gamma: gamma.val
            d: d.val
            l: l.val
        }

        ColumnLayout {
            id: params

            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 10

            Field { id: intencity; label: "Интенсивность источника (I)"; step: 0.1; minVal: 0.0; maxVal: 1; val: 1.0;}
            Field { id: gamma; label: "Длина волны (λ)"; suff: "нм"; minVal: 390; maxVal: 770;}
            Field { id: d; label: "Расстояние между отверстиями (d)"; suff: "м"; step: 0.01; minVal: 0.01; maxVal: 30;}
            Field { id: l; label: "Расстояние до экрана (L)"; suff: "м"; step: 0.01; minVal: 0.01; maxVal: 30;}
        }
    }
}
