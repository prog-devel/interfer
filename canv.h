#ifndef CANV_H
#define CANV_H

#include <QQuickFramebufferObject>
#include <QOpenGLShaderProgram>
#include <QOpenGLFunctions>
#include <QOpenGLTexture>

#define DECLARE_SETTER(type, pname, Pname) \
    private: \
        Q_PROPERTY(type pname READ pname WRITE set##Pname NOTIFY pname##Changed) \
        type m_##pname; \
    Q_SIGNALS: \
        void pname##Changed(); \
    public: \
        inline type pname() const { return m_##pname; } \
        void set##Pname(type);


class Canv: public QQuickFramebufferObject
{
    Q_OBJECT

    DECLARE_SETTER(qreal, intencity, Intencity)
    DECLARE_SETTER(qreal, gamma, Gamma)
    DECLARE_SETTER(qreal, d, D)
    DECLARE_SETTER(qreal, l, L)

public:
    Canv();

    Renderer *createRenderer() const Q_DECL_OVERRIDE;
};

#endif // CANV_H
