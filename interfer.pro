## requires qt5

VERSION = 0.0.1

TEMPLATE = app
CONFIG *= qt
QT += widgets quick qml
CONFIG += c++11 warn_on debug
DEFINES += USE_QRC

DESTDIR = build
OBJECTS_DIR = build/obj
MOC_DIR = build/moc
UI_DIR = build/uic


## Platform-specific
unix {
    HARDWARE_PLATFORM = $$system(uname -i)
    message("Platform: UNIX "$$HARDWARE_PLATFORM)
}
win32 {
    LIBS += -lopengl32
}

## Build pass
build_pass:CONFIG(debug, debug|release) {
}

TARGET = interfer
DEFINES *=
LIBS +=
INCLUDEPATH +=

SOURCES = \
    main.cpp \
    canv.cpp

RESOURCES = interfer.qrc

HEADERS += \
    canv.h

DISTFILES += \
    interfer.fsh \
    interfer.vsh
